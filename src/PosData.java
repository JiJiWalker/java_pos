/**
 * Created by Jakub on 15.04.2017.
 */
public class PosData {
    private String product;
    private Double price;

    public String getProduct() {
        return product;
    }

    Double getPrice() {
        return price;
    }

    PosData(String product, Double price) {
        this.product = product;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PosData posData = (PosData) o;

        return (product != null ? product.equals(posData.product) : posData.product == null) && (price != null ? price.equals(posData.price) : posData.price == null);
    }

    @Override
    public int hashCode() {
        int result = product != null ? product.hashCode() : 0;
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        String text;
        text = "Produkt: "+product+". Cena: "+price+" zł";
        return text;
    }
}
