import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jakub on 15.04.2017.
 */
class PosDataBase {


    HashMap<String, PosData> productDb = new HashMap<>();
    ArrayList<PosData> listDb = new ArrayList<>();

    PosDataBase() {
        new HashMap<>();
    }
//    Pierwsza metoda boolean. Sprawdza czy produkt istnieje i go wyświetla if tak
//    boolean findProduct(String key) { // Było PosData posdata2 jako wartości.
//        return null != productDb.get(key);
//    }

//    druga ze metod boolean. Sprawdzała czy istnieje taki klucz.
//    boolean findEmptyProduct(String key) { // Było PosData posData2 jako wartości. Jest tutaj warunek if
//        return !productDb.containsKey(key);
//    }


//    trzecia z metod boolean. Sprawdzała, czy do kodu kreskowego jest przypisany produkt.
//    Miała złe założenie bo to input miał być pusty (String "*spacja*" albo null (sam enter))
//    A nie klucz pusty w tym sensie, że jest bar-code do którego nie ma produktu
//    boolean findEmptyBarCode(String key) {
//        productDb.get(key);
//        PosData posData2 = new PosData();
//        if (productDb.get(key).price == null || productDb.get(key).product == null) {
//            System.out.println("Invalid bar-code"); // zakryte bo drukuje podwójnie
//            return true;
//        } else {
//            return false;
//        }
//    }

    // metoda działa jak należy (wkońcu!) i zajmuje tylko 8 linijek. 16.04
//    dodać metodę put w wypadku dobrego klucza 16.04
//    po to aby później to wyświetlić i zsumować. 16.04
//    jeżeli poda EXIT. (dodać if'y w else) 16.04
//    -----------------------------------------------------------------
//    metoda była by idealna i dobra, gdyby nie błędne założenie
//    Że empty klucz oznacza empty input a nie nieprzypisany produkt do klucza.
//    Można jej urzyć gdyby założenie okazało się prawidłowe.
//    public void findProduct1(String key) {
//        if (!productDb.containsKey(key)) {
//            System.out.println("*LCD*"+"\n"+"Product not found"+"\n");
//        } else if (null == productDb.get(key).getProduct() || null == productDb.get(key).getPrice()) {
//            System.out.println("*LCD*"+"\n"+"Invalid bar-code"+"\n");
//        } else {
//            System.out.println("*LCD*"+"\n"+productDb.get(key).toString()+"\n");
//
//        }
//    }
//    Kwestia jest nastepująca. Invalid bar-code miał być wyświetlany wtedy
//    Kiedy teoretycznie powinien to łąpać blok try/catch
//    Czyli wtedy, kiedy kod JEST pusty, mianowicie "" albo null (spacja albo enter)
//    Nie oznacza to, że do kodu miał być przypisany pusty kod
//    Pozostaje zmiana kodu, usunięcie konstruktorów pustych, ustawienie większej bazy

    void findProduct(String key) {
        if (key == null || key.equals("")) {
            System.out.println("*LCD*"+"\n"+"Invalid bar-code"+"\n");
        } else {
            if (productDb.containsKey(key)) {
                System.out.println("*LCD*"+"\n"+productDb.get(key).toString()+"\n");
                PosData found = productDb.get(key);
                listDb.add(found);
            } else {
                System.out.println("*LCD*"+"\n"+"Product not found"+"\n");
            }
        }
    }
}

