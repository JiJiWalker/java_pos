import java.util.Scanner;

/**
 * Created by Jakub on 20.04.2017.
 */
public class PosScanner {
    Scanner sc;

    public PosScanner() {
       sc = new Scanner(System.in);
    }

    public void closeScanner() {
        sc.close();
    }

}
