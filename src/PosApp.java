import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Jakub on 15.04.2017.
 */
public class PosApp {

    private static final String SCAN = "scan";
    private static final String EXIT = "exit";

    private PosDataBase posDataBase;

    private PosApp() {
        posDataBase = new PosDataBase();
    }

    public static void main(String[] args) {
        PosApp posApp = new PosApp();
//
        posApp.posDataBase.productDb.put("123", new PosData("Mleko", 3.00));
        posApp.posDataBase.productDb.put("111", new PosData("Pieczywo", 1.59));
        posApp.posDataBase.productDb.put("222", new PosData("Masło", 4.99));
        posApp.posDataBase.productDb.put("666", new PosData("Pizza", 7.89));
        posApp.posDataBase.productDb.put("333", new PosData("Jabłka", 0.48));
        posApp.posDataBase.productDb.put("444", new PosData("Mąka", 2.99));
        posApp.posDataBase.productDb.put("555", new PosData("Oliwa", 10.95));
        posApp.posDataBase.productDb.put("777", new PosData("Musztarda", 5.49));
        posApp.posDataBase.productDb.put("888", new PosData("Majonez", 8.99));

        try (Scanner sc = new Scanner(System.in)) {
            String userOption;
            do {
                posApp.printOption();
                userOption = sc.nextLine();
                if (!userOption.equals(PosApp.SCAN) && !userOption.equals(PosApp.EXIT)) {
                    System.out.println("Wrong command."+"\n");
                }
                switch (userOption) {
                    case PosApp.SCAN:
                        posApp.printProduct(sc);
                        break;
                    case PosApp.EXIT:
                        posApp.printProductOnLCD();
                        posApp.printPrices(posApp.posDataBase.listDb);
                        break;
                }
            } while (!userOption.equals(PosApp.EXIT));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

//    metoda, która sprawdza czy produkt jest ok i go zwracam
//    private void printProduct(Scanner sc) {
//        System.out.println("*next product*");
//        String key = sc.nextLine();
//        System.out.println(posDataBase.productDb.get(key)+"\n");
//    }

//    metoda sprawdzająca czy klucz istnieje, jeżeli nie zwraca txt
//    private void printEmptyElement(Scanner sc) {
//        System.out.println("*next empty element*");
//        String key = sc.nextLine();
//        if (!posDataBase.productDb.containsKey(key)) {
//            System.out.println("Product not found"+"\n");
//        }
//    }

//    metoda sprawdzająca czy produkt ma puste wartości, jeżeli tak to drukuje txt
//    private void printEmptyBarCode(Scanner sc) {
//        System.out.println("*next invalid bar-code*");
//        PosData posData2 = new PosData();
//
//        String key = sc.nextLine();
//        posDataBase.productDb.get(key);
//        if (posData2.getProduct() == null || posData2.getPrice() == null) {
//            System.out.println("Invalid bar-code"+"\n");
//        } else {
//            System.out.println("Good code.");
//        }
//    }

//    połączenie powyższych metod + metod z klasy PosDataBase
//    private void printAllProduct(Scanner sc) {
//        System.out.println("*next all product*");
//        String key = sc.nextLine();
//        if (posDataBase.findProduct(key)) {
//            System.out.println(posDataBase.productDb.get(key)+"\n");
//        } else if (posDataBase.findEmptyProduct(key)) {
//            System.out.println("Product not found"+"\n");
//        } else if (posDataBase.findEmptyBarCode(key)) {
//            System.out.println("Invalid bar-code"+"\n");
//        } else {
//            System.out.println("Dupa blada.");
//        }
//    }

    private void printProduct(Scanner sc) {
        System.out.println("*next product*");
        String key = sc.nextLine();
        System.out.println("");
        posDataBase.findProduct(key);
    }

    private void printOption() {
        System.out.println("\t"+"***Point Of Sale***");
        System.out.println("Insert 'scan' (then bar-code) to scan product");
        System.out.println("Inster 'exit' to finalize transaction.");
    }

    private void printProductOnLCD() throws InterruptedException {
        System.out.println("");
        System.out.println("*Printer*");
        PosPrinter posPrinter = new PosPrinter();
        posPrinter.printerLCD();
        Thread.sleep(1000);
        for (PosData f: posDataBase.listDb) {
            System.out.println(f.toString());
        }
    }

    private void printPrices(ArrayList<PosData> arrayList) {
        StringBuilder sb = new StringBuilder();
        Double sum = 0.0;
        for (PosData f: arrayList) {
            sum += f.getPrice();
        }
        sb.append(sum);
        System.out.println("Sum: "+sum+"\n");
        System.out.println("*LCD*");
        System.out.println("Sum: "+sum);
    }

}
